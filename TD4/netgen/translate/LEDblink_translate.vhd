--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: LEDblink_translate.vhd
-- /___/   /\     Timestamp: MON 7 DEC 10:55:3 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -tpw 0 -ar Structure -tm LEDblink -w -dir netgen/translate -ofmt vhdl -sim LEDblink.ngd LEDblink_translate.vhd 
-- Device	: 7a100tcsg324-3
-- Input file	: LEDblink.ngd
-- Output file	: D:\WorkSpace\computer2\td4\netgen\translate\LEDblink_translate.vhd
-- # of Entities	: 1
-- Design Name	: LEDblink
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity LEDblink is
  port (
    clk : in STD_LOGIC := 'X'; 
    A : out STD_LOGIC_VECTOR ( 15 downto 0 ); 
    B : out STD_LOGIC_VECTOR ( 15 downto 0 ); 
    C : out STD_LOGIC_VECTOR ( 15 downto 0 ) 
  );
end LEDblink;

architecture Structure of LEDblink is
  signal clk_BUFGP : STD_LOGIC; 
  signal carry_28 : STD_LOGIC; 
  signal A_15_OBUF_34 : STD_LOGIC; 
  signal A_3_35 : STD_LOGIC; 
  signal A_2_36 : STD_LOGIC; 
  signal A_1_37 : STD_LOGIC; 
  signal A_0_38 : STD_LOGIC; 
  signal PC_3_PC_3_wide_mux_24_OUT_3_Q : STD_LOGIC; 
  signal PC_3_PC_3_wide_mux_24_OUT_2_Q : STD_LOGIC; 
  signal PC_3_PC_3_wide_mux_24_OUT_1_Q : STD_LOGIC; 
  signal PC_3_PC_3_wide_mux_24_OUT_0_Q : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o : STD_LOGIC; 
  signal PC_3_GND_5_o_Mux_23_o : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal Q_n0230_inv : STD_LOGIC; 
  signal Mram_n00363 : STD_LOGIC; 
  signal Mram_n00366 : STD_LOGIC; 
  signal Mmux_n0211_rs_xor_3_12_98 : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o_19_Q : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o_19_1_100 : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o_19_2_101 : STD_LOGIC; 
  signal Mcount_counter_cy_1_rt_151 : STD_LOGIC; 
  signal Mcount_counter_cy_2_rt_152 : STD_LOGIC; 
  signal Mcount_counter_cy_3_rt_153 : STD_LOGIC; 
  signal Mcount_counter_cy_4_rt_154 : STD_LOGIC; 
  signal Mcount_counter_cy_5_rt_155 : STD_LOGIC; 
  signal Mcount_counter_cy_6_rt_156 : STD_LOGIC; 
  signal Mcount_counter_cy_7_rt_157 : STD_LOGIC; 
  signal Mcount_counter_cy_8_rt_158 : STD_LOGIC; 
  signal Mcount_counter_cy_9_rt_159 : STD_LOGIC; 
  signal Mcount_counter_cy_10_rt_160 : STD_LOGIC; 
  signal Mcount_counter_cy_11_rt_161 : STD_LOGIC; 
  signal Mcount_counter_cy_12_rt_162 : STD_LOGIC; 
  signal Mcount_counter_cy_13_rt_163 : STD_LOGIC; 
  signal Mcount_counter_cy_14_rt_164 : STD_LOGIC; 
  signal Mcount_counter_cy_15_rt_165 : STD_LOGIC; 
  signal Mcount_counter_cy_16_rt_166 : STD_LOGIC; 
  signal Mcount_counter_cy_17_rt_167 : STD_LOGIC; 
  signal Mcount_counter_cy_18_rt_168 : STD_LOGIC; 
  signal Mcount_counter_xor_19_rt_169 : STD_LOGIC; 
  signal A_0_dpot_170 : STD_LOGIC; 
  signal A_1_dpot_171 : STD_LOGIC; 
  signal A_2_dpot_172 : STD_LOGIC; 
  signal A_3_dpot_173 : STD_LOGIC; 
  signal regA_0_dpot_174 : STD_LOGIC; 
  signal regA_1_dpot_175 : STD_LOGIC; 
  signal regA_2_dpot_176 : STD_LOGIC; 
  signal regA_3_dpot_177 : STD_LOGIC; 
  signal tempA_0_dpot_178 : STD_LOGIC; 
  signal tempA_2_dpot_179 : STD_LOGIC; 
  signal tempA_1_dpot_180 : STD_LOGIC; 
  signal N4 : STD_LOGIC; 
  signal N6 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal N14 : STD_LOGIC; 
  signal N15 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal N17 : STD_LOGIC; 
  signal N18 : STD_LOGIC; 
  signal N19 : STD_LOGIC; 
  signal clk_BUFGP_IBUFG_2 : STD_LOGIC; 
  signal GND : STD_LOGIC; 
  signal VCC : STD_LOGIC; 
  signal counter : STD_LOGIC_VECTOR ( 19 downto 0 ); 
  signal regA : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal tempA : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal Q_n0211 : STD_LOGIC_VECTOR ( 3 downto 2 ); 
  signal Result : STD_LOGIC_VECTOR ( 19 downto 0 ); 
  signal PC : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Mcount_counter_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal Mcount_counter_cy : STD_LOGIC_VECTOR ( 18 downto 0 ); 
  signal Mmux_n0211_rs_lut : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal Mmux_n0211_rs_cy : STD_LOGIC_VECTOR ( 3 downto 3 ); 
begin
  XST_VCC : X_ONE
    port map (
      O => N0
    );
  XST_GND : X_ZERO
    port map (
      O => A_15_OBUF_34
    );
  PC_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => PC_3_PC_3_wide_mux_24_OUT_0_Q,
      O => PC(0),
      SET => GND,
      RST => GND
    );
  PC_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => PC_3_PC_3_wide_mux_24_OUT_1_Q,
      O => PC(1),
      SET => GND,
      RST => GND
    );
  PC_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => PC_3_PC_3_wide_mux_24_OUT_2_Q,
      O => PC(2),
      SET => GND,
      RST => GND
    );
  PC_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => PC_3_PC_3_wide_mux_24_OUT_3_Q,
      O => PC(3),
      SET => GND,
      RST => GND
    );
  A_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => A_0_dpot_170,
      O => A_0_38,
      SET => GND,
      RST => GND
    );
  A_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => A_1_dpot_171,
      O => A_1_37,
      SET => GND,
      RST => GND
    );
  A_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => A_2_dpot_172,
      O => A_2_36,
      SET => GND,
      RST => GND
    );
  A_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => A_3_dpot_173,
      O => A_3_35,
      SET => GND,
      RST => GND
    );
  tempA_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => tempA_0_dpot_178,
      O => tempA(0),
      SET => GND,
      RST => GND
    );
  tempA_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => tempA_1_dpot_180,
      O => tempA(1),
      SET => GND,
      RST => GND
    );
  tempA_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => tempA_2_dpot_179,
      O => tempA(2),
      SET => GND,
      RST => GND
    );
  tempA_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => Q_n0230_inv,
      I => Q_n0211(3),
      O => tempA(3),
      SET => GND,
      RST => GND
    );
  tempA_4 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => Q_n0230_inv,
      I => Mmux_n0211_rs_cy(3),
      O => tempA(4),
      SET => GND,
      RST => GND
    );
  regA_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => regA_0_dpot_174,
      O => regA(0),
      SET => GND,
      RST => GND
    );
  regA_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => regA_1_dpot_175,
      O => regA(1),
      SET => GND,
      RST => GND
    );
  regA_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => regA_2_dpot_176,
      O => regA(2),
      SET => GND,
      RST => GND
    );
  regA_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => regA_3_dpot_177,
      O => regA(3),
      SET => GND,
      RST => GND
    );
  carry : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      CE => counter_19_GND_5_o_equal_2_o,
      I => PC_3_GND_5_o_Mux_23_o,
      O => carry_28,
      SET => GND,
      RST => GND
    );
  counter_0 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(0),
      O => counter(0),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_1 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(1),
      O => counter(1),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_2 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(2),
      O => counter(2),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_3 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(3),
      O => counter(3),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_4 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(4),
      O => counter(4),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_5 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(5),
      O => counter(5),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_6 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(6),
      O => counter(6),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_7 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(7),
      O => counter(7),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_8 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(8),
      O => counter(8),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_9 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(9),
      O => counter(9),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_10 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(10),
      O => counter(10),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_11 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(11),
      O => counter(11),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_12 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(12),
      O => counter(12),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_13 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(13),
      O => counter(13),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_14 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(14),
      O => counter(14),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_15 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(15),
      O => counter(15),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_16 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(16),
      O => counter(16),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_17 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(17),
      O => counter(17),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_18 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(18),
      O => counter(18),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  counter_19 : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      CLK => clk_BUFGP,
      I => Result(19),
      O => counter(19),
      CE => VCC,
      SET => GND,
      RST => GND
    );
  Mcount_counter_cy_0_Q : X_MUX2
    port map (
      IB => A_15_OBUF_34,
      IA => N0,
      SEL => Mcount_counter_lut(0),
      O => Mcount_counter_cy(0)
    );
  Mcount_counter_xor_0_Q : X_XOR2
    port map (
      I0 => A_15_OBUF_34,
      I1 => Mcount_counter_lut(0),
      O => Result(0)
    );
  Mcount_counter_cy_1_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(0),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_1_rt_151,
      O => Mcount_counter_cy(1)
    );
  Mcount_counter_xor_1_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(0),
      I1 => Mcount_counter_cy_1_rt_151,
      O => Result(1)
    );
  Mcount_counter_cy_2_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(1),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_2_rt_152,
      O => Mcount_counter_cy(2)
    );
  Mcount_counter_xor_2_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(1),
      I1 => Mcount_counter_cy_2_rt_152,
      O => Result(2)
    );
  Mcount_counter_cy_3_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(2),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_3_rt_153,
      O => Mcount_counter_cy(3)
    );
  Mcount_counter_xor_3_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(2),
      I1 => Mcount_counter_cy_3_rt_153,
      O => Result(3)
    );
  Mcount_counter_cy_4_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(3),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_4_rt_154,
      O => Mcount_counter_cy(4)
    );
  Mcount_counter_xor_4_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(3),
      I1 => Mcount_counter_cy_4_rt_154,
      O => Result(4)
    );
  Mcount_counter_cy_5_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(4),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_5_rt_155,
      O => Mcount_counter_cy(5)
    );
  Mcount_counter_xor_5_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(4),
      I1 => Mcount_counter_cy_5_rt_155,
      O => Result(5)
    );
  Mcount_counter_cy_6_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(5),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_6_rt_156,
      O => Mcount_counter_cy(6)
    );
  Mcount_counter_xor_6_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(5),
      I1 => Mcount_counter_cy_6_rt_156,
      O => Result(6)
    );
  Mcount_counter_cy_7_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(6),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_7_rt_157,
      O => Mcount_counter_cy(7)
    );
  Mcount_counter_xor_7_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(6),
      I1 => Mcount_counter_cy_7_rt_157,
      O => Result(7)
    );
  Mcount_counter_cy_8_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(7),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_8_rt_158,
      O => Mcount_counter_cy(8)
    );
  Mcount_counter_xor_8_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(7),
      I1 => Mcount_counter_cy_8_rt_158,
      O => Result(8)
    );
  Mcount_counter_cy_9_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(8),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_9_rt_159,
      O => Mcount_counter_cy(9)
    );
  Mcount_counter_xor_9_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(8),
      I1 => Mcount_counter_cy_9_rt_159,
      O => Result(9)
    );
  Mcount_counter_cy_10_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(9),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_10_rt_160,
      O => Mcount_counter_cy(10)
    );
  Mcount_counter_xor_10_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(9),
      I1 => Mcount_counter_cy_10_rt_160,
      O => Result(10)
    );
  Mcount_counter_cy_11_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(10),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_11_rt_161,
      O => Mcount_counter_cy(11)
    );
  Mcount_counter_xor_11_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(10),
      I1 => Mcount_counter_cy_11_rt_161,
      O => Result(11)
    );
  Mcount_counter_cy_12_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(11),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_12_rt_162,
      O => Mcount_counter_cy(12)
    );
  Mcount_counter_xor_12_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(11),
      I1 => Mcount_counter_cy_12_rt_162,
      O => Result(12)
    );
  Mcount_counter_cy_13_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(12),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_13_rt_163,
      O => Mcount_counter_cy(13)
    );
  Mcount_counter_xor_13_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(12),
      I1 => Mcount_counter_cy_13_rt_163,
      O => Result(13)
    );
  Mcount_counter_cy_14_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(13),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_14_rt_164,
      O => Mcount_counter_cy(14)
    );
  Mcount_counter_xor_14_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(13),
      I1 => Mcount_counter_cy_14_rt_164,
      O => Result(14)
    );
  Mcount_counter_cy_15_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(14),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_15_rt_165,
      O => Mcount_counter_cy(15)
    );
  Mcount_counter_xor_15_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(14),
      I1 => Mcount_counter_cy_15_rt_165,
      O => Result(15)
    );
  Mcount_counter_cy_16_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(15),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_16_rt_166,
      O => Mcount_counter_cy(16)
    );
  Mcount_counter_xor_16_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(15),
      I1 => Mcount_counter_cy_16_rt_166,
      O => Result(16)
    );
  Mcount_counter_cy_17_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(16),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_17_rt_167,
      O => Mcount_counter_cy(17)
    );
  Mcount_counter_xor_17_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(16),
      I1 => Mcount_counter_cy_17_rt_167,
      O => Result(17)
    );
  Mcount_counter_cy_18_Q : X_MUX2
    port map (
      IB => Mcount_counter_cy(17),
      IA => A_15_OBUF_34,
      SEL => Mcount_counter_cy_18_rt_168,
      O => Mcount_counter_cy(18)
    );
  Mcount_counter_xor_18_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(17),
      I1 => Mcount_counter_cy_18_rt_168,
      O => Result(18)
    );
  Mcount_counter_xor_19_Q : X_XOR2
    port map (
      I0 => Mcount_counter_cy(18),
      I1 => Mcount_counter_xor_19_rt_169,
      O => Result(19)
    );
  Mmux_PC_3_PC_3_wide_mux_24_OUT31 : X_LUT5
    generic map(
      INIT => X"6A6A62E8"
    )
    port map (
      ADR0 => PC(2),
      ADR1 => PC(0),
      ADR2 => PC(1),
      ADR3 => PC(3),
      ADR4 => carry_28,
      O => PC_3_PC_3_wide_mux_24_OUT_2_Q
    );
  Mmux_PC_3_PC_3_wide_mux_24_OUT21 : X_LUT5
    generic map(
      INIT => X"666664F2"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => PC(1),
      ADR2 => PC(2),
      ADR3 => PC(3),
      ADR4 => carry_28,
      O => PC_3_PC_3_wide_mux_24_OUT_1_Q
    );
  Mram_n003661 : X_LUT4
    generic map(
      INIT => X"A294"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => PC(1),
      ADR2 => PC(2),
      ADR3 => PC(3),
      O => Mram_n00366
    );
  Mmux_PC_3_PC_3_wide_mux_24_OUT41 : X_LUT5
    generic map(
      INIT => X"6AAA2AAA"
    )
    port map (
      ADR0 => PC(3),
      ADR1 => PC(0),
      ADR2 => PC(1),
      ADR3 => PC(2),
      ADR4 => carry_28,
      O => PC_3_PC_3_wide_mux_24_OUT_3_Q
    );
  Mram_n003631 : X_LUT4
    generic map(
      INIT => X"2808"
    )
    port map (
      ADR0 => PC(3),
      ADR1 => PC(0),
      ADR2 => PC(1),
      ADR3 => PC(2),
      O => Mram_n00363
    );
  counter_19_GND_5_o_equal_2_o_19_1 : X_LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      ADR0 => counter(1),
      ADR1 => counter(0),
      ADR2 => counter(2),
      ADR3 => counter(3),
      ADR4 => counter(4),
      ADR5 => counter(5),
      O => counter_19_GND_5_o_equal_2_o_19_Q
    );
  counter_19_GND_5_o_equal_2_o_19_2 : X_LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      ADR0 => counter(7),
      ADR1 => counter(6),
      ADR2 => counter(8),
      ADR3 => counter(9),
      ADR4 => counter(10),
      ADR5 => counter(11),
      O => counter_19_GND_5_o_equal_2_o_19_1_100
    );
  counter_19_GND_5_o_equal_2_o_19_3 : X_LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      ADR0 => counter(13),
      ADR1 => counter(12),
      ADR2 => counter(14),
      ADR3 => counter(15),
      ADR4 => counter(16),
      ADR5 => counter(17),
      O => counter_19_GND_5_o_equal_2_o_19_2_101
    );
  counter_19_GND_5_o_equal_2_o_19_4 : X_LUT5
    generic map(
      INIT => X"10000000"
    )
    port map (
      ADR0 => counter(18),
      ADR1 => counter(19),
      ADR2 => counter_19_GND_5_o_equal_2_o_19_2_101,
      ADR3 => counter_19_GND_5_o_equal_2_o_19_1_100,
      ADR4 => counter_19_GND_5_o_equal_2_o_19_Q,
      O => counter_19_GND_5_o_equal_2_o
    );
  Mcount_counter_cy_1_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(1),
      O => Mcount_counter_cy_1_rt_151,
      ADR1 => GND
    );
  Mcount_counter_cy_2_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(2),
      O => Mcount_counter_cy_2_rt_152,
      ADR1 => GND
    );
  Mcount_counter_cy_3_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(3),
      O => Mcount_counter_cy_3_rt_153,
      ADR1 => GND
    );
  Mcount_counter_cy_4_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(4),
      O => Mcount_counter_cy_4_rt_154,
      ADR1 => GND
    );
  Mcount_counter_cy_5_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(5),
      O => Mcount_counter_cy_5_rt_155,
      ADR1 => GND
    );
  Mcount_counter_cy_6_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(6),
      O => Mcount_counter_cy_6_rt_156,
      ADR1 => GND
    );
  Mcount_counter_cy_7_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(7),
      O => Mcount_counter_cy_7_rt_157,
      ADR1 => GND
    );
  Mcount_counter_cy_8_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(8),
      O => Mcount_counter_cy_8_rt_158,
      ADR1 => GND
    );
  Mcount_counter_cy_9_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(9),
      O => Mcount_counter_cy_9_rt_159,
      ADR1 => GND
    );
  Mcount_counter_cy_10_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(10),
      O => Mcount_counter_cy_10_rt_160,
      ADR1 => GND
    );
  Mcount_counter_cy_11_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(11),
      O => Mcount_counter_cy_11_rt_161,
      ADR1 => GND
    );
  Mcount_counter_cy_12_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(12),
      O => Mcount_counter_cy_12_rt_162,
      ADR1 => GND
    );
  Mcount_counter_cy_13_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(13),
      O => Mcount_counter_cy_13_rt_163,
      ADR1 => GND
    );
  Mcount_counter_cy_14_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(14),
      O => Mcount_counter_cy_14_rt_164,
      ADR1 => GND
    );
  Mcount_counter_cy_15_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(15),
      O => Mcount_counter_cy_15_rt_165,
      ADR1 => GND
    );
  Mcount_counter_cy_16_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(16),
      O => Mcount_counter_cy_16_rt_166,
      ADR1 => GND
    );
  Mcount_counter_cy_17_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(17),
      O => Mcount_counter_cy_17_rt_167,
      ADR1 => GND
    );
  Mcount_counter_cy_18_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(18),
      O => Mcount_counter_cy_18_rt_168,
      ADR1 => GND
    );
  Mcount_counter_xor_19_rt : X_LUT2
    generic map(
      INIT => X"A"
    )
    port map (
      ADR0 => counter(19),
      O => Mcount_counter_xor_19_rt_169,
      ADR1 => GND
    );
  Mmux_n0211_rs_xor_3_11 : X_LUT4
    generic map(
      INIT => X"4BB4"
    )
    port map (
      ADR0 => Mram_n00366,
      ADR1 => regA(3),
      ADR2 => Mram_n00363,
      ADR3 => Mmux_n0211_rs_xor_3_12_98,
      O => Q_n0211(3)
    );
  Mmux_n0211_rs_lut_1_1 : X_LUT5
    generic map(
      INIT => X"794CEE05"
    )
    port map (
      ADR0 => PC(3),
      ADR1 => PC(2),
      ADR2 => PC(1),
      ADR3 => regA(1),
      ADR4 => PC(0),
      O => Mmux_n0211_rs_lut(1)
    );
  Mmux_n0211_rs_lut_0_1 : X_LUT5
    generic map(
      INIT => X"4C34115F"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => PC(1),
      ADR2 => PC(2),
      ADR3 => PC(3),
      ADR4 => regA(0),
      O => Mmux_n0211_rs_lut(0)
    );
  Mmux_n007411_SW0 : X_LUT5
    generic map(
      INIT => X"EFCEAE8E"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => PC(3),
      ADR2 => tempA(0),
      ADR3 => PC(2),
      ADR4 => PC(1),
      O => N4
    );
  tempA_1_dpot_SW0 : X_LUT4
    generic map(
      INIT => X"F9AD"
    )
    port map (
      ADR0 => PC(3),
      ADR1 => PC(1),
      ADR2 => PC(0),
      ADR3 => PC(2),
      O => N6
    );
  tempA_1_dpot : X_LUT6
    generic map(
      INIT => X"AAAFAAA0AAA3AAAC"
    )
    port map (
      ADR0 => tempA(1),
      ADR1 => regA(0),
      ADR2 => N6,
      ADR3 => Mram_n00366,
      ADR4 => Mmux_n0211_rs_lut(1),
      ADR5 => Mmux_n0211_rs_lut(0),
      O => tempA_1_dpot_180
    );
  Q_n0230_inv1 : X_LUT6
    generic map(
      INIT => X"0100000000000000"
    )
    port map (
      ADR0 => counter(18),
      ADR1 => counter(19),
      ADR2 => N6,
      ADR3 => counter_19_GND_5_o_equal_2_o_19_Q,
      ADR4 => counter_19_GND_5_o_equal_2_o_19_1_100,
      ADR5 => counter_19_GND_5_o_equal_2_o_19_2_101,
      O => Q_n0230_inv
    );
  tempA_2_dpot : X_LUT6
    generic map(
      INIT => X"ABAEBABEA8A28A82"
    )
    port map (
      ADR0 => tempA(2),
      ADR1 => PC(3),
      ADR2 => PC(0),
      ADR3 => PC(1),
      ADR4 => PC(2),
      ADR5 => Q_n0211(2),
      O => tempA_2_dpot_179
    );
  regA_3_dpot : X_LUT6
    generic map(
      INIT => X"ABAEBABEA8A28A82"
    )
    port map (
      ADR0 => regA(3),
      ADR1 => PC(0),
      ADR2 => PC(3),
      ADR3 => PC(2),
      ADR4 => PC(1),
      ADR5 => tempA(3),
      O => regA_3_dpot_177
    );
  Mmux_n0211_rs_cy_3_11 : X_LUT6
    generic map(
      INIT => X"008000000AE8EE8A"
    )
    port map (
      ADR0 => regA(3),
      ADR1 => PC(3),
      ADR2 => PC(2),
      ADR3 => PC(0),
      ADR4 => PC(1),
      ADR5 => N12,
      O => Mmux_n0211_rs_cy(3)
    );
  Mmux_PC_3_GND_5_o_Mux_23_o11 : X_LUT5
    generic map(
      INIT => X"02200828"
    )
    port map (
      ADR0 => tempA(4),
      ADR1 => PC(0),
      ADR2 => PC(3),
      ADR3 => PC(1),
      ADR4 => PC(2),
      O => PC_3_GND_5_o_Mux_23_o
    );
  regA_1_dpot : X_LUT6
    generic map(
      INIT => X"ABAEBABEA8A28A82"
    )
    port map (
      ADR0 => regA(1),
      ADR1 => PC(0),
      ADR2 => PC(3),
      ADR3 => PC(2),
      ADR4 => PC(1),
      ADR5 => tempA(1),
      O => regA_1_dpot_175
    );
  regA_2_dpot : X_LUT6
    generic map(
      INIT => X"ABAEBABEA8A28A82"
    )
    port map (
      ADR0 => regA(2),
      ADR1 => PC(0),
      ADR2 => PC(3),
      ADR3 => PC(2),
      ADR4 => PC(1),
      ADR5 => tempA(2),
      O => regA_2_dpot_176
    );
  A_0_dpot : X_LUT5
    generic map(
      INIT => X"A2228AAB"
    )
    port map (
      ADR0 => A_0_38,
      ADR1 => PC(1),
      ADR2 => PC(2),
      ADR3 => PC(0),
      ADR4 => PC(3),
      O => A_0_dpot_170
    );
  A_1_dpot : X_LUT5
    generic map(
      INIT => X"F7771001"
    )
    port map (
      ADR0 => PC(1),
      ADR1 => PC(3),
      ADR2 => PC(0),
      ADR3 => PC(2),
      ADR4 => A_1_37,
      O => A_1_dpot_171
    );
  A_2_dpot : X_LUT5
    generic map(
      INIT => X"F7F71081"
    )
    port map (
      ADR0 => PC(1),
      ADR1 => PC(3),
      ADR2 => PC(0),
      ADR3 => PC(2),
      ADR4 => A_2_36,
      O => A_2_dpot_172
    );
  tempA_0_dpot : X_LUT6
    generic map(
      INIT => X"AA8288A2AABEBBAE"
    )
    port map (
      ADR0 => tempA(0),
      ADR1 => PC(0),
      ADR2 => PC(2),
      ADR3 => PC(3),
      ADR4 => PC(1),
      ADR5 => regA(0),
      O => tempA_0_dpot_178
    );
  A_3_dpot : X_LUT5
    generic map(
      INIT => X"A82AEA28"
    )
    port map (
      ADR0 => A_3_35,
      ADR1 => PC(1),
      ADR2 => PC(3),
      ADR3 => PC(2),
      ADR4 => PC(0),
      O => A_3_dpot_173
    );
  regA_0_dpot : X_LUT6
    generic map(
      INIT => X"AA8288A2AABEBBAE"
    )
    port map (
      ADR0 => regA(0),
      ADR1 => PC(0),
      ADR2 => PC(2),
      ADR3 => PC(3),
      ADR4 => PC(1),
      ADR5 => N4,
      O => regA_0_dpot_174
    );
  Mmux_n0211_rs_cy_3_11_SW0 : X_MUX2
    port map (
      IA => N14,
      IB => N15,
      SEL => PC(3),
      O => N12
    );
  Mmux_n0211_rs_cy_3_11_SW0_F : X_LUT6
    generic map(
      INIT => X"9FFFFFFF46666E6F"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => PC(2),
      ADR2 => regA(2),
      ADR3 => regA(0),
      ADR4 => regA(1),
      ADR5 => PC(1),
      O => N14
    );
  Mmux_n0211_rs_cy_3_11_SW0_G : X_LUT6
    generic map(
      INIT => X"EFFF6777FFFFFFFF"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => PC(1),
      ADR2 => regA(0),
      ADR3 => regA(1),
      ADR4 => PC(2),
      ADR5 => regA(2),
      O => N15
    );
  Mmux_n0211_rs_xor_3_12 : X_MUX2
    port map (
      IA => N16,
      IB => N17,
      SEL => PC(3),
      O => Mmux_n0211_rs_xor_3_12_98
    );
  Mmux_n0211_rs_xor_3_12_F : X_LUT6
    generic map(
      INIT => X"7941410141414100"
    )
    port map (
      ADR0 => PC(1),
      ADR1 => PC(2),
      ADR2 => PC(0),
      ADR3 => regA(1),
      ADR4 => regA(2),
      ADR5 => regA(0),
      O => N16
    );
  Mmux_n0211_rs_xor_3_12_G : X_LUT6
    generic map(
      INIT => X"200A200020002000"
    )
    port map (
      ADR0 => regA(2),
      ADR1 => PC(2),
      ADR2 => PC(0),
      ADR3 => PC(1),
      ADR4 => regA(0),
      ADR5 => regA(1),
      O => N17
    );
  Mmux_n0211_rs_xor_2_11 : X_MUX2
    port map (
      IA => N18,
      IB => N19,
      SEL => PC(3),
      O => Q_n0211(2)
    );
  Mmux_n0211_rs_xor_2_11_F : X_LUT6
    generic map(
      INIT => X"D9E6EAE642828281"
    )
    port map (
      ADR0 => regA(2),
      ADR1 => PC(2),
      ADR2 => PC(1),
      ADR3 => regA(1),
      ADR4 => regA(0),
      ADR5 => PC(0),
      O => N18
    );
  Mmux_n0211_rs_xor_2_11_G : X_LUT6
    generic map(
      INIT => X"4414444466146644"
    )
    port map (
      ADR0 => PC(0),
      ADR1 => regA(2),
      ADR2 => regA(0),
      ADR3 => PC(1),
      ADR4 => regA(1),
      ADR5 => PC(2),
      O => N19
    );
  Mcount_counter_lut_0_INV_0 : X_INV
    port map (
      I => counter(0),
      O => Mcount_counter_lut(0)
    );
  Mmux_PC_3_PC_3_wide_mux_24_OUT11_INV_0 : X_INV
    port map (
      I => PC(0),
      O => PC_3_PC_3_wide_mux_24_OUT_0_Q
    );
  clk_BUFGP_BUFG : X_CKBUF
    port map (
      I => clk_BUFGP_IBUFG_2,
      O => clk_BUFGP
    );
  clk_BUFGP_IBUFG : X_CKBUF
    port map (
      I => clk,
      O => clk_BUFGP_IBUFG_2
    );
  A_15_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(15)
    );
  A_14_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(14)
    );
  A_13_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(13)
    );
  A_12_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(12)
    );
  A_11_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(11)
    );
  A_10_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(10)
    );
  A_9_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(9)
    );
  A_8_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(8)
    );
  A_7_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(7)
    );
  A_6_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(6)
    );
  A_5_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(5)
    );
  A_4_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => A(4)
    );
  A_3_OBUF : X_OBUF
    port map (
      I => A_3_35,
      O => A(3)
    );
  A_2_OBUF : X_OBUF
    port map (
      I => A_2_36,
      O => A(2)
    );
  A_1_OBUF : X_OBUF
    port map (
      I => A_1_37,
      O => A(1)
    );
  A_0_OBUF : X_OBUF
    port map (
      I => A_0_38,
      O => A(0)
    );
  B_15_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(15)
    );
  B_14_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(14)
    );
  B_13_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(13)
    );
  B_12_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(12)
    );
  B_11_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(11)
    );
  B_10_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(10)
    );
  B_9_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(9)
    );
  B_8_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(8)
    );
  B_7_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(7)
    );
  B_6_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(6)
    );
  B_5_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(5)
    );
  B_4_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(4)
    );
  B_3_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(3)
    );
  B_2_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(2)
    );
  B_1_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(1)
    );
  B_0_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => B(0)
    );
  C_15_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(15)
    );
  C_14_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(14)
    );
  C_13_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(13)
    );
  C_12_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(12)
    );
  C_11_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(11)
    );
  C_10_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(10)
    );
  C_9_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(9)
    );
  C_8_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(8)
    );
  C_7_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(7)
    );
  C_6_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(6)
    );
  C_5_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(5)
    );
  C_4_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(4)
    );
  C_3_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(3)
    );
  C_2_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(2)
    );
  C_1_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(1)
    );
  C_0_OBUF : X_OBUF
    port map (
      I => A_15_OBUF_34,
      O => C(0)
    );
  NlwBlock_LEDblink_GND : X_ZERO
    port map (
      O => GND
    );
  NlwBlock_LEDblink_VCC : X_ONE
    port map (
      O => VCC
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;


--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: LEDblink_synthesis.vhd
-- /___/   /\     Timestamp: MON 14 DEC 11:12:24 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm LEDblink -w -dir netgen/synthesis -ofmt vhdl -sim LEDblink.ngc LEDblink_synthesis.vhd 
-- Device	: xc7a100t-3-csg324
-- Input file	: LEDblink.ngc
-- Output file	: D:\WorkSpace\computer2\td4\netgen\synthesis\LEDblink_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: LEDblink
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity LEDblink is
  port (
    clk : in STD_LOGIC := 'X'; 
    A : out STD_LOGIC_VECTOR ( 15 downto 0 ); 
    B : out STD_LOGIC_VECTOR ( 15 downto 0 ); 
    C : out STD_LOGIC_VECTOR ( 15 downto 0 ) 
  );
end LEDblink;

architecture Structure of LEDblink is
  signal clk_BUFGP_0 : STD_LOGIC; 
  signal A_15_OBUF_33 : STD_LOGIC; 
  signal A_3_34 : STD_LOGIC; 
  signal A_2_35 : STD_LOGIC; 
  signal A_1_36 : STD_LOGIC; 
  signal A_0_37 : STD_LOGIC; 
  signal tempA_38 : STD_LOGIC; 
  signal \Q_n0133_2)\ : STD_LOGIC; 
  signal \Q_n0133_0)\ : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal Q_n0287_inv : STD_LOGIC; 
  signal Mram_n00531 : STD_LOGIC; 
  signal Mram_n00536 : STD_LOGIC; 
  signal Mmux_n013311 : STD_LOGIC; 
  signal Mmux_n0268_rs_xor_3_11_98 : STD_LOGIC; 
  signal Q_n0236_inv1 : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o_19_Q : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o_19_1_101 : STD_LOGIC; 
  signal counter_19_GND_5_o_equal_2_o_19_2_102 : STD_LOGIC; 
  signal N01 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal Mcount_counter_cy_1_rt_154 : STD_LOGIC; 
  signal Mcount_counter_cy_2_rt_155 : STD_LOGIC; 
  signal Mcount_counter_cy_3_rt_156 : STD_LOGIC; 
  signal Mcount_counter_cy_4_rt_157 : STD_LOGIC; 
  signal Mcount_counter_cy_5_rt_158 : STD_LOGIC; 
  signal Mcount_counter_cy_6_rt_159 : STD_LOGIC; 
  signal Mcount_counter_cy_7_rt_160 : STD_LOGIC; 
  signal Mcount_counter_cy_8_rt_161 : STD_LOGIC; 
  signal Mcount_counter_cy_9_rt_162 : STD_LOGIC; 
  signal Mcount_counter_cy_10_rt_163 : STD_LOGIC; 
  signal Mcount_counter_cy_11_rt_164 : STD_LOGIC; 
  signal Mcount_counter_cy_12_rt_165 : STD_LOGIC; 
  signal Mcount_counter_cy_13_rt_166 : STD_LOGIC; 
  signal Mcount_counter_cy_14_rt_167 : STD_LOGIC; 
  signal Mcount_counter_cy_15_rt_168 : STD_LOGIC; 
  signal Mcount_counter_cy_16_rt_169 : STD_LOGIC; 
  signal Mcount_counter_cy_17_rt_170 : STD_LOGIC; 
  signal Mcount_counter_cy_18_rt_171 : STD_LOGIC; 
  signal Mcount_counter_xor_19_rt_172 : STD_LOGIC; 
  signal N4 : STD_LOGIC; 
  signal N5 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal N13 : STD_LOGIC; 
  signal regA_0_dpot_177 : STD_LOGIC; 
  signal regA_1_dpot_178 : STD_LOGIC; 
  signal regA_2_dpot_179 : STD_LOGIC; 
  signal regA_3_dpot_180 : STD_LOGIC; 
  signal PC_0_dpot_181 : STD_LOGIC; 
  signal PC_1_dpot_182 : STD_LOGIC; 
  signal PC_2_dpot_183 : STD_LOGIC; 
  signal PC_3_dpot_184 : STD_LOGIC; 
  signal temp_0_dpot_185 : STD_LOGIC; 
  signal regB_0_dpot : STD_LOGIC; 
  signal regB_1_dpot : STD_LOGIC; 
  signal regB_2_dpot : STD_LOGIC; 
  signal regB_3_dpot : STD_LOGIC; 
  signal Q_n0307_inv1_rstpot_190 : STD_LOGIC; 
  signal A_0_dpot_191 : STD_LOGIC; 
  signal A_1_dpot_192 : STD_LOGIC; 
  signal A_2_dpot_193 : STD_LOGIC; 
  signal A_3_dpot_194 : STD_LOGIC; 
  signal tempA_dpot_195 : STD_LOGIC; 
  signal tempB_1_rstpot_196 : STD_LOGIC; 
  signal tempC_2_dpot_197 : STD_LOGIC; 
  signal temp_1_dpot_198 : STD_LOGIC; 
  signal temp_2_dpot_199 : STD_LOGIC; 
  signal counter : STD_LOGIC_VECTOR ( 19 downto 0 ); 
  signal regA : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal regB : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal temp : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal tempB : STD_LOGIC_VECTOR ( 1 downto 1 ); 
  signal tempC : STD_LOGIC_VECTOR ( 2 downto 2 ); 
  signal Q_n0268 : STD_LOGIC_VECTOR ( 3 downto 1 ); 
  signal Result : STD_LOGIC_VECTOR ( 19 downto 0 ); 
  signal PC : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Mcount_counter_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal Mcount_counter_cy : STD_LOGIC_VECTOR ( 18 downto 0 ); 
  signal Mmux_n0268_rs_lut : STD_LOGIC_VECTOR ( 1 downto 0 ); 
begin
  XST_VCC : VCC
    port map (
      P => N0
    );
  XST_GND : GND
    port map (
      G => A_15_OBUF_33
    );
  tempA : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => tempA_dpot_195,
      Q => tempA_38
    );
  tempC_2 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => tempC_2_dpot_197,
      Q => tempC(2)
    );
  PC_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => PC_0_dpot_181,
      Q => PC(0)
    );
  PC_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => PC_1_dpot_182,
      Q => PC(1)
    );
  PC_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => PC_2_dpot_183,
      Q => PC(2)
    );
  PC_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => PC_3_dpot_184,
      Q => PC(3)
    );
  regB_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regB_0_dpot,
      Q => regB(0)
    );
  regB_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regB_1_dpot,
      Q => regB(1)
    );
  regB_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regB_2_dpot,
      Q => regB(2)
    );
  regB_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regB_3_dpot,
      Q => regB(3)
    );
  A_0 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => A_0_dpot_191,
      Q => A_0_37
    );
  A_1 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => A_1_dpot_192,
      Q => A_1_36
    );
  A_2 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => A_2_dpot_193,
      Q => A_2_35
    );
  A_3 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => A_3_dpot_194,
      Q => A_3_34
    );
  temp_0 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => temp_0_dpot_185,
      Q => temp(0)
    );
  temp_1 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => temp_1_dpot_198,
      Q => temp(1)
    );
  temp_2 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => temp_2_dpot_199,
      Q => temp(2)
    );
  temp_3 : FDE
    port map (
      C => clk_BUFGP_0,
      CE => Q_n0287_inv,
      D => Q_n0268(3),
      Q => temp(3)
    );
  regA_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regA_0_dpot_177,
      Q => regA(0)
    );
  regA_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regA_1_dpot_178,
      Q => regA(1)
    );
  regA_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regA_2_dpot_179,
      Q => regA(2)
    );
  regA_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      CE => counter_19_GND_5_o_equal_2_o,
      D => regA_3_dpot_180,
      Q => regA(3)
    );
  counter_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(0),
      Q => counter(0)
    );
  counter_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(1),
      Q => counter(1)
    );
  counter_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(2),
      Q => counter(2)
    );
  counter_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(3),
      Q => counter(3)
    );
  counter_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(4),
      Q => counter(4)
    );
  counter_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(5),
      Q => counter(5)
    );
  counter_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(6),
      Q => counter(6)
    );
  counter_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(7),
      Q => counter(7)
    );
  counter_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(8),
      Q => counter(8)
    );
  counter_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(9),
      Q => counter(9)
    );
  counter_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(10),
      Q => counter(10)
    );
  counter_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(11),
      Q => counter(11)
    );
  counter_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(12),
      Q => counter(12)
    );
  counter_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(13),
      Q => counter(13)
    );
  counter_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(14),
      Q => counter(14)
    );
  counter_15 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(15),
      Q => counter(15)
    );
  counter_16 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(16),
      Q => counter(16)
    );
  counter_17 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(17),
      Q => counter(17)
    );
  counter_18 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(18),
      Q => counter(18)
    );
  counter_19 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_0,
      D => Result(19),
      Q => counter(19)
    );
  Mcount_counter_cy_0_Q : MUXCY
    port map (
      CI => A_15_OBUF_33,
      DI => N0,
      S => Mcount_counter_lut(0),
      O => Mcount_counter_cy(0)
    );
  Mcount_counter_xor_0_Q : XORCY
    port map (
      CI => A_15_OBUF_33,
      LI => Mcount_counter_lut(0),
      O => Result(0)
    );
  Mcount_counter_cy_1_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(0),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_1_rt_154,
      O => Mcount_counter_cy(1)
    );
  Mcount_counter_xor_1_Q : XORCY
    port map (
      CI => Mcount_counter_cy(0),
      LI => Mcount_counter_cy_1_rt_154,
      O => Result(1)
    );
  Mcount_counter_cy_2_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(1),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_2_rt_155,
      O => Mcount_counter_cy(2)
    );
  Mcount_counter_xor_2_Q : XORCY
    port map (
      CI => Mcount_counter_cy(1),
      LI => Mcount_counter_cy_2_rt_155,
      O => Result(2)
    );
  Mcount_counter_cy_3_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(2),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_3_rt_156,
      O => Mcount_counter_cy(3)
    );
  Mcount_counter_xor_3_Q : XORCY
    port map (
      CI => Mcount_counter_cy(2),
      LI => Mcount_counter_cy_3_rt_156,
      O => Result(3)
    );
  Mcount_counter_cy_4_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(3),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_4_rt_157,
      O => Mcount_counter_cy(4)
    );
  Mcount_counter_xor_4_Q : XORCY
    port map (
      CI => Mcount_counter_cy(3),
      LI => Mcount_counter_cy_4_rt_157,
      O => Result(4)
    );
  Mcount_counter_cy_5_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(4),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_5_rt_158,
      O => Mcount_counter_cy(5)
    );
  Mcount_counter_xor_5_Q : XORCY
    port map (
      CI => Mcount_counter_cy(4),
      LI => Mcount_counter_cy_5_rt_158,
      O => Result(5)
    );
  Mcount_counter_cy_6_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(5),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_6_rt_159,
      O => Mcount_counter_cy(6)
    );
  Mcount_counter_xor_6_Q : XORCY
    port map (
      CI => Mcount_counter_cy(5),
      LI => Mcount_counter_cy_6_rt_159,
      O => Result(6)
    );
  Mcount_counter_cy_7_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(6),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_7_rt_160,
      O => Mcount_counter_cy(7)
    );
  Mcount_counter_xor_7_Q : XORCY
    port map (
      CI => Mcount_counter_cy(6),
      LI => Mcount_counter_cy_7_rt_160,
      O => Result(7)
    );
  Mcount_counter_cy_8_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(7),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_8_rt_161,
      O => Mcount_counter_cy(8)
    );
  Mcount_counter_xor_8_Q : XORCY
    port map (
      CI => Mcount_counter_cy(7),
      LI => Mcount_counter_cy_8_rt_161,
      O => Result(8)
    );
  Mcount_counter_cy_9_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(8),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_9_rt_162,
      O => Mcount_counter_cy(9)
    );
  Mcount_counter_xor_9_Q : XORCY
    port map (
      CI => Mcount_counter_cy(8),
      LI => Mcount_counter_cy_9_rt_162,
      O => Result(9)
    );
  Mcount_counter_cy_10_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(9),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_10_rt_163,
      O => Mcount_counter_cy(10)
    );
  Mcount_counter_xor_10_Q : XORCY
    port map (
      CI => Mcount_counter_cy(9),
      LI => Mcount_counter_cy_10_rt_163,
      O => Result(10)
    );
  Mcount_counter_cy_11_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(10),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_11_rt_164,
      O => Mcount_counter_cy(11)
    );
  Mcount_counter_xor_11_Q : XORCY
    port map (
      CI => Mcount_counter_cy(10),
      LI => Mcount_counter_cy_11_rt_164,
      O => Result(11)
    );
  Mcount_counter_cy_12_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(11),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_12_rt_165,
      O => Mcount_counter_cy(12)
    );
  Mcount_counter_xor_12_Q : XORCY
    port map (
      CI => Mcount_counter_cy(11),
      LI => Mcount_counter_cy_12_rt_165,
      O => Result(12)
    );
  Mcount_counter_cy_13_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(12),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_13_rt_166,
      O => Mcount_counter_cy(13)
    );
  Mcount_counter_xor_13_Q : XORCY
    port map (
      CI => Mcount_counter_cy(12),
      LI => Mcount_counter_cy_13_rt_166,
      O => Result(13)
    );
  Mcount_counter_cy_14_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(13),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_14_rt_167,
      O => Mcount_counter_cy(14)
    );
  Mcount_counter_xor_14_Q : XORCY
    port map (
      CI => Mcount_counter_cy(13),
      LI => Mcount_counter_cy_14_rt_167,
      O => Result(14)
    );
  Mcount_counter_cy_15_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(14),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_15_rt_168,
      O => Mcount_counter_cy(15)
    );
  Mcount_counter_xor_15_Q : XORCY
    port map (
      CI => Mcount_counter_cy(14),
      LI => Mcount_counter_cy_15_rt_168,
      O => Result(15)
    );
  Mcount_counter_cy_16_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(15),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_16_rt_169,
      O => Mcount_counter_cy(16)
    );
  Mcount_counter_xor_16_Q : XORCY
    port map (
      CI => Mcount_counter_cy(15),
      LI => Mcount_counter_cy_16_rt_169,
      O => Result(16)
    );
  Mcount_counter_cy_17_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(16),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_17_rt_170,
      O => Mcount_counter_cy(17)
    );
  Mcount_counter_xor_17_Q : XORCY
    port map (
      CI => Mcount_counter_cy(16),
      LI => Mcount_counter_cy_17_rt_170,
      O => Result(17)
    );
  Mcount_counter_cy_18_Q : MUXCY
    port map (
      CI => Mcount_counter_cy(17),
      DI => A_15_OBUF_33,
      S => Mcount_counter_cy_18_rt_171,
      O => Mcount_counter_cy(18)
    );
  Mcount_counter_xor_18_Q : XORCY
    port map (
      CI => Mcount_counter_cy(17),
      LI => Mcount_counter_cy_18_rt_171,
      O => Result(18)
    );
  Mcount_counter_xor_19_Q : XORCY
    port map (
      CI => Mcount_counter_cy(18),
      LI => Mcount_counter_xor_19_rt_172,
      O => Result(19)
    );
  Mmux_n0133111 : LUT4
    generic map(
      INIT => X"FFFD"
    )
    port map (
      I0 => PC(1),
      I1 => PC(3),
      I2 => PC(0),
      I3 => PC(2),
      O => Mmux_n013311
    );
  Q_n0236_inv11 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => PC(3),
      I1 => PC(0),
      I2 => PC(2),
      O => Q_n0236_inv1
    );
  Q_n0263_inv11 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => PC(2),
      I1 => PC(1),
      I2 => PC(3),
      O => Mram_n00531
    );
  Mmux_n0268_rs_xor_3_11 : LUT6
    generic map(
      INIT => X"F0F0AAAA0FF06666"
    )
    port map (
      I0 => regA(3),
      I1 => regA(2),
      I2 => regB(3),
      I3 => regB(2),
      I4 => Mram_n00536,
      I5 => Mmux_n0268_rs_xor_3_11_98,
      O => Q_n0268(3)
    );
  counter_19_GND_5_o_equal_2_o_19_1 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => counter(1),
      I1 => counter(0),
      I2 => counter(2),
      I3 => counter(3),
      I4 => counter(4),
      I5 => counter(5),
      O => counter_19_GND_5_o_equal_2_o_19_Q
    );
  counter_19_GND_5_o_equal_2_o_19_2 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => counter(7),
      I1 => counter(6),
      I2 => counter(8),
      I3 => counter(9),
      I4 => counter(10),
      I5 => counter(11),
      O => counter_19_GND_5_o_equal_2_o_19_1_101
    );
  counter_19_GND_5_o_equal_2_o_19_3 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => counter(13),
      I1 => counter(12),
      I2 => counter(14),
      I3 => counter(15),
      I4 => counter(16),
      I5 => counter(17),
      O => counter_19_GND_5_o_equal_2_o_19_2_102
    );
  counter_19_GND_5_o_equal_2_o_19_4 : LUT5
    generic map(
      INIT => X"10000000"
    )
    port map (
      I0 => counter(18),
      I1 => counter(19),
      I2 => counter_19_GND_5_o_equal_2_o_19_2_102,
      I3 => counter_19_GND_5_o_equal_2_o_19_1_101,
      I4 => counter_19_GND_5_o_equal_2_o_19_Q,
      O => counter_19_GND_5_o_equal_2_o
    );
  Mmux_n01331_SW0 : LUT4
    generic map(
      INIT => X"CCCA"
    )
    port map (
      I0 => tempA_38,
      I1 => temp(0),
      I2 => PC(0),
      I3 => PC(3),
      O => N01
    );
  Mmux_n01331 : LUT6
    generic map(
      INIT => X"CCCCCCCCAAFAAA0A"
    )
    port map (
      I0 => temp(0),
      I1 => regB(0),
      I2 => PC(1),
      I3 => PC(2),
      I4 => N01,
      I5 => Mram_n00536,
      O => \Q_n0133_0)\
    );
  Mmux_n01333_SW0 : LUT4
    generic map(
      INIT => X"CCCA"
    )
    port map (
      I0 => tempC(2),
      I1 => temp(2),
      I2 => PC(0),
      I3 => PC(3),
      O => N2
    );
  Mmux_n01333 : LUT6
    generic map(
      INIT => X"CCCCCCCCAAFAAA0A"
    )
    port map (
      I0 => temp(2),
      I1 => regB(2),
      I2 => PC(1),
      I3 => PC(2),
      I4 => N2,
      I5 => Mram_n00536,
      O => \Q_n0133_2)\
    );
  A_15_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(15)
    );
  A_14_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(14)
    );
  A_13_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(13)
    );
  A_12_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(12)
    );
  A_11_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(11)
    );
  A_10_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(10)
    );
  A_9_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(9)
    );
  A_8_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(8)
    );
  A_7_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(7)
    );
  A_6_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(6)
    );
  A_5_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(5)
    );
  A_4_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => A(4)
    );
  A_3_OBUF : OBUF
    port map (
      I => A_3_34,
      O => A(3)
    );
  A_2_OBUF : OBUF
    port map (
      I => A_2_35,
      O => A(2)
    );
  A_1_OBUF : OBUF
    port map (
      I => A_1_36,
      O => A(1)
    );
  A_0_OBUF : OBUF
    port map (
      I => A_0_37,
      O => A(0)
    );
  B_15_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(15)
    );
  B_14_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(14)
    );
  B_13_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(13)
    );
  B_12_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(12)
    );
  B_11_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(11)
    );
  B_10_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(10)
    );
  B_9_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(9)
    );
  B_8_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(8)
    );
  B_7_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(7)
    );
  B_6_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(6)
    );
  B_5_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(5)
    );
  B_4_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(4)
    );
  B_3_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(3)
    );
  B_2_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(2)
    );
  B_1_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(1)
    );
  B_0_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => B(0)
    );
  C_15_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(15)
    );
  C_14_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(14)
    );
  C_13_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(13)
    );
  C_12_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(12)
    );
  C_11_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(11)
    );
  C_10_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(10)
    );
  C_9_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(9)
    );
  C_8_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(8)
    );
  C_7_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(7)
    );
  C_6_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(6)
    );
  C_5_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(5)
    );
  C_4_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(4)
    );
  C_3_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(3)
    );
  C_2_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(2)
    );
  C_1_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(1)
    );
  C_0_OBUF : OBUF
    port map (
      I => A_15_OBUF_33,
      O => C(0)
    );
  Mcount_counter_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(1),
      O => Mcount_counter_cy_1_rt_154
    );
  Mcount_counter_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(2),
      O => Mcount_counter_cy_2_rt_155
    );
  Mcount_counter_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(3),
      O => Mcount_counter_cy_3_rt_156
    );
  Mcount_counter_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(4),
      O => Mcount_counter_cy_4_rt_157
    );
  Mcount_counter_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(5),
      O => Mcount_counter_cy_5_rt_158
    );
  Mcount_counter_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(6),
      O => Mcount_counter_cy_6_rt_159
    );
  Mcount_counter_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(7),
      O => Mcount_counter_cy_7_rt_160
    );
  Mcount_counter_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(8),
      O => Mcount_counter_cy_8_rt_161
    );
  Mcount_counter_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(9),
      O => Mcount_counter_cy_9_rt_162
    );
  Mcount_counter_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(10),
      O => Mcount_counter_cy_10_rt_163
    );
  Mcount_counter_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(11),
      O => Mcount_counter_cy_11_rt_164
    );
  Mcount_counter_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(12),
      O => Mcount_counter_cy_12_rt_165
    );
  Mcount_counter_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(13),
      O => Mcount_counter_cy_13_rt_166
    );
  Mcount_counter_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(14),
      O => Mcount_counter_cy_14_rt_167
    );
  Mcount_counter_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(15),
      O => Mcount_counter_cy_15_rt_168
    );
  Mcount_counter_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(16),
      O => Mcount_counter_cy_16_rt_169
    );
  Mcount_counter_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(17),
      O => Mcount_counter_cy_17_rt_170
    );
  Mcount_counter_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(18),
      O => Mcount_counter_cy_18_rt_171
    );
  Mcount_counter_xor_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => counter(19),
      O => Mcount_counter_xor_19_rt_172
    );
  Mmux_n0268_rs_xor_3_111 : LUT6
    generic map(
      INIT => X"3F5F3F05035F0305"
    )
    port map (
      I0 => regA(1),
      I1 => regB(1),
      I2 => Mram_n00531,
      I3 => Mram_n00536,
      I4 => N4,
      I5 => N5,
      O => Mmux_n0268_rs_xor_3_11_98
    );
  Mram_n005361 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => PC(0),
      I1 => PC(2),
      I2 => PC(1),
      I3 => PC(3),
      O => Mram_n00536
    );
  Mmux_n0268_rs_xor_3_111_SW0 : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => regA(0),
      I1 => PC(1),
      I2 => PC(3),
      I3 => PC(0),
      I4 => PC(2),
      O => N4
    );
  Mmux_n0268_rs_xor_3_111_SW1 : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => regB(0),
      I1 => PC(1),
      I2 => PC(3),
      I3 => PC(0),
      I4 => PC(2),
      O => N5
    );
  Mmux_n0268_rs_xor_1_11 : LUT5
    generic map(
      INIT => X"FF0035CA"
    )
    port map (
      I0 => regA(0),
      I1 => regB(0),
      I2 => Mram_n00536,
      I3 => Mmux_n0268_rs_lut(1),
      I4 => Mmux_n0268_rs_lut(0),
      O => Q_n0268(1)
    );
  Mmux_n0268_rs_xor_2_11 : LUT6
    generic map(
      INIT => X"3C335555AAAAC3CC"
    )
    port map (
      I0 => regA(2),
      I1 => regB(2),
      I2 => regB(1),
      I3 => N5,
      I4 => N13,
      I5 => N12,
      O => Q_n0268(2)
    );
  Mmux_n0268_rs_lut_0_1 : LUT6
    generic map(
      INIT => X"CCCCCCCCCCCCCCA3"
    )
    port map (
      I0 => regB(0),
      I1 => regA(0),
      I2 => PC(0),
      I3 => PC(3),
      I4 => PC(2),
      I5 => PC(1),
      O => Mmux_n0268_rs_lut(0)
    );
  Mmux_n0268_rs_lut_1_1 : LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAA35"
    )
    port map (
      I0 => regA(1),
      I1 => regB(1),
      I2 => PC(0),
      I3 => PC(3),
      I4 => PC(1),
      I5 => PC(2),
      O => Mmux_n0268_rs_lut(1)
    );
  tempB_1 : FD
    port map (
      C => clk_BUFGP_0,
      D => tempB_1_rstpot_196,
      Q => tempB(1)
    );
  tempB_1_rstpot : LUT6
    generic map(
      INIT => X"AAAAC0AAAAAAAAAA"
    )
    port map (
      I0 => tempB(1),
      I1 => regA(1),
      I2 => regB(1),
      I3 => PC(1),
      I4 => Q_n0236_inv1,
      I5 => counter_19_GND_5_o_equal_2_o,
      O => tempB_1_rstpot_196
    );
  regA_1_dpot : LUT6
    generic map(
      INIT => X"CCF0CCAACCF0CC00"
    )
    port map (
      I0 => temp(1),
      I1 => regA(1),
      I2 => regB(1),
      I3 => Q_n0236_inv1,
      I4 => Mram_n00536,
      I5 => Mmux_n013311,
      O => regA_1_dpot_178
    );
  regA_3_dpot : LUT6
    generic map(
      INIT => X"AAF0AACCAAF0AA00"
    )
    port map (
      I0 => regA(3),
      I1 => temp(3),
      I2 => regB(3),
      I3 => Q_n0236_inv1,
      I4 => Mram_n00536,
      I5 => Mmux_n013311,
      O => regA_3_dpot_180
    );
  A_0_dpot : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Q_n0307_inv1_rstpot_190,
      I1 => regB(0),
      I2 => Mmux_n013311,
      I3 => A_0_37,
      O => A_0_dpot_191
    );
  A_1_dpot : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Q_n0307_inv1_rstpot_190,
      I1 => regB(1),
      I2 => Mmux_n013311,
      I3 => A_1_36,
      O => A_1_dpot_192
    );
  A_2_dpot : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Q_n0307_inv1_rstpot_190,
      I1 => regB(2),
      I2 => Mmux_n013311,
      I3 => A_2_35,
      O => A_2_dpot_193
    );
  A_3_dpot : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Q_n0307_inv1_rstpot_190,
      I1 => regB(3),
      I2 => Mmux_n013311,
      I3 => A_3_34,
      O => A_3_dpot_194
    );
  Mmux_n0268_rs_xor_2_11_SW0 : LUT6
    generic map(
      INIT => X"00000000000000FE"
    )
    port map (
      I0 => regA(1),
      I1 => regA(0),
      I2 => PC(0),
      I3 => PC(3),
      I4 => PC(2),
      I5 => PC(1),
      O => N12
    );
  Mmux_n0268_rs_xor_2_11_SW1 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF1"
    )
    port map (
      I0 => regA(1),
      I1 => regA(0),
      I2 => PC(0),
      I3 => PC(3),
      I4 => PC(1),
      I5 => PC(2),
      O => N13
    );
  Q_n0307_inv1_rstpot : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => PC(2),
      I1 => PC(0),
      I2 => PC(1),
      I3 => PC(3),
      O => Q_n0307_inv1_rstpot_190
    );
  temp_1_dpot : LUT5
    generic map(
      INIT => X"AAABAAA8"
    )
    port map (
      I0 => temp(1),
      I1 => PC(2),
      I2 => PC(1),
      I3 => PC(3),
      I4 => Q_n0268(1),
      O => temp_1_dpot_198
    );
  regA_0_dpot : LUT5
    generic map(
      INIT => X"AAABAAA8"
    )
    port map (
      I0 => regA(0),
      I1 => PC(3),
      I2 => PC(0),
      I3 => PC(2),
      I4 => \Q_n0133_0)\,
      O => regA_0_dpot_177
    );
  regA_2_dpot : LUT5
    generic map(
      INIT => X"AAABAAA8"
    )
    port map (
      I0 => regA(2),
      I1 => PC(3),
      I2 => PC(0),
      I3 => PC(2),
      I4 => \Q_n0133_2)\,
      O => regA_2_dpot_179
    );
  Q_n0287_inv1 : LUT6
    generic map(
      INIT => X"1000000000000000"
    )
    port map (
      I0 => counter(18),
      I1 => counter(19),
      I2 => Mram_n00531,
      I3 => counter_19_GND_5_o_equal_2_o_19_Q,
      I4 => counter_19_GND_5_o_equal_2_o_19_2_102,
      I5 => counter_19_GND_5_o_equal_2_o_19_1_101,
      O => Q_n0287_inv
    );
  temp_2_dpot : LUT5
    generic map(
      INIT => X"AAABAAA8"
    )
    port map (
      I0 => temp(2),
      I1 => PC(2),
      I2 => PC(1),
      I3 => PC(3),
      I4 => Q_n0268(2),
      O => temp_2_dpot_199
    );
  tempA_dpot : LUT5
    generic map(
      INIT => X"AEA2A2A2"
    )
    port map (
      I0 => tempA_38,
      I1 => PC(1),
      I2 => Q_n0236_inv1,
      I3 => regA(0),
      I4 => regB(0),
      O => tempA_dpot_195
    );
  PC_1_dpot : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => PC(1),
      I1 => PC(0),
      O => PC_1_dpot_182
    );
  PC_2_dpot : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => PC(2),
      I1 => PC(1),
      I2 => PC(0),
      O => PC_2_dpot_183
    );
  PC_3_dpot : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => PC(3),
      I1 => PC(1),
      I2 => PC(2),
      I3 => PC(0),
      O => PC_3_dpot_184
    );
  PC_0_dpot : LUT4
    generic map(
      INIT => X"5455"
    )
    port map (
      I0 => PC(0),
      I1 => PC(2),
      I2 => PC(3),
      I3 => PC(1),
      O => PC_0_dpot_181
    );
  tempC_2_dpot : LUT6
    generic map(
      INIT => X"AAABAAAAAAA8AAAA"
    )
    port map (
      I0 => tempC(2),
      I1 => PC(3),
      I2 => PC(0),
      I3 => PC(2),
      I4 => PC(1),
      I5 => tempB(1),
      O => tempC_2_dpot_197
    );
  temp_0_dpot : LUT5
    generic map(
      INIT => X"AAABAAA8"
    )
    port map (
      I0 => temp(0),
      I1 => PC(2),
      I2 => PC(1),
      I3 => PC(3),
      I4 => Mmux_n0268_rs_lut(0),
      O => temp_0_dpot_185
    );
  regB_0_dpot1 : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Mram_n00536,
      I1 => temp(0),
      I2 => Mmux_n013311,
      I3 => regB(0),
      O => regB_0_dpot
    );
  regB_1_dpot1 : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Mram_n00536,
      I1 => temp(1),
      I2 => Mmux_n013311,
      I3 => regB(1),
      O => regB_1_dpot
    );
  regB_2_dpot1 : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Mram_n00536,
      I1 => temp(2),
      I2 => Mmux_n013311,
      I3 => regB(2),
      O => regB_2_dpot
    );
  regB_3_dpot1 : LUT4
    generic map(
      INIT => X"D580"
    )
    port map (
      I0 => Mram_n00536,
      I1 => temp(3),
      I2 => Mmux_n013311,
      I3 => regB(3),
      O => regB_3_dpot
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_0
    );
  Mcount_counter_lut_0_INV_0 : INV
    port map (
      I => counter(0),
      O => Mcount_counter_lut(0)
    );

end Structure;


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:20:19 11/09/2015 
-- Design Name: 
-- Module Name:    TD4_main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--
---- Uncomment the following library declaration if using
---- arithmetic functions with Signed or Unsigned values
----use IEEE.NUMERIC_STD.ALL;
--
---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
----library UNISIM;
----use UNISIM.VComponents.all;
--
--entity TD4_main is
--	Port (CLOCK : in STD_LOGIC;
--			RESET : in STD_LOGIC;
--			Input : in STD_LOGIC_Vector (3 downto 0);
--			Output : out STD_LOGIC_Vector (3 downto 0));
--	
--end TD4_main;
--
--architecture Behavioral of TD4_main is
--		A : STD_LOGIC_Vector (3 downto 0);
--		B : STD_LOGIC_Vector (3 downto 0);
--		PC : STD_LOGIC_Vector (3 downto 0);
--begin
--
--
--end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity LEDblink is
  Port ( A : out  STD_LOGIC_VECTOR (15 downto 0);
         B : out  STD_LOGIC_VECTOR (15 downto 0);
         C : out  STD_LOGIC_VECTOR (15 downto 0);
         clk : in STD_LOGIC);
end LEDblink;

architecture Behavioral of LEDblink is
  signal counter : STD_LOGIC_VECTOR(19 downto 0) := (others => '0');
  signal PC : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
  signal regA : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
  signal regB : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
  signal carry : STD_LOGIC := '0';
  signal temp : STD_LOGIC_VECTOR(4 downto 0); 
  signal tempA : STD_LOGIC_VECTOR(3 downto 0); 
  signal tempB : STD_LOGIC_VECTOR(3 downto 0); 
  signal tempC : STD_LOGIC_VECTOR(3 downto 0); 
  subtype operation is std_logic_vector (7 downto 0);
  type MEMORY is array (0 to 15) of operation;
  constant RAM : MEMORY := (
--    "10110111",
--    "00000001",
--    "11100001",
--    "00000001",
--    "11100011",
--    "10110110",
--    "00000001",
--    "11100110",
--    "00000001",
--    "11101000",
--    "10110000",
--    "10110100",
--    "00000001",
--    "11101010",
--    "10111000",
--    "11110000"
	"01010010",
	"00000011",
	"00100000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000",
	"10010000"
  );
begin
  process(clk)begin
    if rising_edge(clk) then
      counter <= counter+'1';
--        if counter = "00000000000000000000" then
           case RAM(conv_integer(PC))(7 downto 4) is
			  when "0011" =>
                 regA <= RAM(conv_integer(PC))(3 downto 0);
                 carry <= '0';
                 PC <= PC+'1';
				when "0111" =>
                regB <= RAM(conv_integer(PC))(3 downto 0);
                carry <= '0';
                PC <= PC+'1';
            when "0001" => 
                regB <= regA;
                carry <= '0';
                PC <= PC+'1';
            when "0100" =>
                regA <= regB;
                carry <= '0';
                PC <= PC+'1';
            when "1111" => 
                PC <= RAM(conv_integer(PC))(3 downto 0);
                carry <= '0';
            when "1110" =>
					if carry = '0' then 
						PC <= RAM(conv_integer(PC))(3 downto 0);
               else
						PC <= PC + '1';
               end if;
               carry <= '0';
            when "0000" => 
                temp <= ('0' & regA) + ('0' & RAM(conv_integer(PC))(3 downto 0));
                regA <= temp(3 downto 0);
                carry <= temp(4);
                PC <= PC+'1';
            when "0101" =>
                temp <= ('0' & regB) + ('0' & RAM(conv_integer(PC))(3 downto 0));
                regB <= temp(3 downto 0);
                carry <= temp(4);
                PC <= PC+'1';
				when "1011" =>
                A <= "000000000000"&RAM(conv_integer(PC))(3 downto 0);
                carry <= '0';
                PC <= PC+'1';
            when "1001" =>
                A <= "000000000000"&regB;
                carry <= '0';
                PC <= PC+'1';
				when "0010" =>
--					tempA <=regA and (regB and "0001");
--					tempB <=regA and (regB and "0010");
--					tempC <= SHL(tempB,"1");
--					regA <= tempA+tempC;
				tempA <= regB and "0001";
				PC <= PC+'1';
--				when "0110" =>
--				;
--				when "1000" =>
--				;
--				when "1010" =>
--				;
--				when "1100" =>
--				;
--				when "1101" =>
--				;
				when others =>
                null;
				end case;
--         end if;
    end if;
  end process;
  B <= "0000000000000000";
  C <= "0000000000000000";
end Behavioral;

